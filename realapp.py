try:
	import simplejson as json
except ImportError:
	try:
		import json
	except ImportError:
		raise ImportError

from flask import Flask, Response, request, render_template, make_response
from bson import json_util
from bson.objectid import ObjectId
from flask.ext.pymongo import PyMongo
import datetime


app = Flask(__name__)
app.config['MONGO_DBNAME'] = 'test'
mongo = PyMongo(app, config_prefix='MONGO')

class MongoJsonEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, (datetime.datetime, datetime.date)):
			return obj.isoformat()
		elif isinstance(obj, ObjectId):
			return unicode(obj)
		return json.JSONEncoder.default(self, obj)

def jsonify(*args, **kwargs):
	return Response(json.dumps(dict(*args, **kwargs), cls=MongoJsonEncoder), mimetype='application/json')

@app.route('/api/list')
def list():
	json_docs = []
	cursor = mongo.db.listings.find().limit(1000)
	for doc in cursor:
		json_docs.append(doc)
	return jsonify(listings = json_docs)

@app.route('/api/list/<city>/<int:min>/<int:max>')
def listCity(city, min, max):
	json_docs = []
	searchObj = { '$and': [ {'City':city, 'Price': {'$gt':min, '$lt':max}} ] }
	#print searchObj
	cursor = mongo.db.listings.find(searchObj)
	for doc in cursor:
		json_docs.append(doc)
	return jsonify(listings = json_docs)

@app.route('/api/list/<float:minlat>/<float:maxlat>/<float:minlong>/<float:maxlong>/<int:limit>/<int:page>')
def lllist(minlat, maxlat, minlong, maxlong, limit, page):
	json_docs = []
	searchObj = { '$and': [{'Latitude':{'$gt':minlat-1000, '$lt':maxlat-1000 }, 'Longitude':{'$gt':minlong-1000, '$lt':maxlong-1000}}]}
	if request.args.get('minPrice'):
		searchObj['$and'][0]['Price'] = {}
		searchObj['$and'][0]['Price']['$gt'] = int(request.args.get('minPrice'))
	if request.args.get('maxPrice'):
		if not hasattr(searchObj['$and'][0],'Price'):
			searchObj['$and'][0]['Price'] = {}
		searchObj['$and'][0]['Price']['$lt'] = int(request.args.get('maxPrice'))
	if request.args.get('city'):
		searchObj['$and'][0]['City'] = request.args.get('city')
	#print searchObj
	cursor = mongo.db.listings.find(searchObj).skip((page-1)*limit).limit(limit)
	totalCount = cursor.count()
	count = cursor.count(True)
	for doc in cursor:
		json_docs.append(doc)
	return jsonify(listings = json_docs, count = count, totalCount = totalCount)

@app.route('/api/cities')
def cities():
	json_docs = []
	cursor = mongo.db.listings.distinct('City')
	for doc in cursor:
		json_docs.append(doc)
	return jsonify(cities = json_docs)

if __name__ == '__main__':
	app.run()
