import sys
import requests
import re
import operator
import datetime
import json
import pymongo
from pymongo import MongoClient
import datetime

sys.setrecursionlimit(20000)

client = MongoClient('localhost', 27017)
db = client.test
totalfound = 0
totalprocessed = 0
totaladded = 0
listings = db.listings

def processlist(result, found):
	global totalfound
	global totalprocessed
	global totaladded
	totalfoundprocessed = 0
	for listing in result["MapSearchResults"]:
		totalprocessed += 1
		totalfoundprocessed += 1
		#print(listing["PropertyID"], listing["Price"], listing['Address'], listing["City"])
		listing['Price'] = listing['Price'].replace("$","").replace(",","").replace(",","").strip()
		listing['Price'] = int(listing['Price'])
		listing['Longitude'] = float(listing['Longitude'])
		listing['Latitude'] = float(listing['Latitude'])
		listing["DateAdded"] = datetime.datetime.now()
		#currentListing = listings.find_one({'PropertyID':listing['PropertyID']})
		#if currentListing is None:
		try:
			listings.insert(listing)
			totaladded += 1
		except:
			continue
	print('%02.3f' % (totalprocessed/totalfound*100),'\tTAdded/TProcessed/TFound>Processed/Found: ', totaladded, '/', totalfound ,'/', totalprocessed,'>',totalfoundprocessed ,'/',found)
		
def mapsearch(minlat,maxlat,minlong,maxlong,splitlat):
	global totalfound
	global totalprocessed
	url = ('http://www.realtor.ca/handlers/MapSearchHandler.ashx?xml=%3CListingSearchMap%3E%3CCulture%3Een-CA%3C/Culture%3E%3COrderBy%3E1%3C/OrderBy%3E%3COrderDirection%3EA%3C/OrderDirection%3E%3CCulture%3Een-CA%3C/Culture%3E%3CLatitudeMax%3E' + 
		('%1.15f' % maxlat) + '%3C/LatitudeMax%3E%3CLatitudeMin%3E' + 
		('%1.15f' % minlat) + '%3C/LatitudeMin%3E%3CLongitudeMax%3E' + 
		('%1.15f' % maxlong) + '%3C/LongitudeMax%3E%3CLongitudeMin%3E' + 
		('%1.15f' % minlong) + '%3C/LongitudeMin%3E%3CPriceMax%3E0%3C/PriceMax%3E%3CPriceMin%3E0%3C/PriceMin%3E%3CPropertyTypeID%3E300%3C/PropertyTypeID%3E%3CTransactionTypeID%3E2%3C/TransactionTypeID%3E%3CMinBath%3E0%3C/MinBath%3E%3CMaxBath%3E0%3C/MaxBath%3E%3CMinBed%3E0%3C/MinBed%3E%3CMaxBed%3E0%3C/MaxBed%3E%3CStoriesTotalMin%3E0%3C/StoriesTotalMin%3E%3CStoriesTotalMax%3E0%3C/StoriesTotalMax%3E%3C/ListingSearchMap%3E')
	try:
		res = requests.get(url)
	#except:
	#	print(url)
	#	print("Unexpected error:", sys.exc_info()[0])
	#try:
	#	if res is not None:
		result = json.loads(res.text.replace('\\', ''))
		found = result["NumberSearchResults"]
		#mapped = result["MappableListingsNotFound"]
	
		if totalfound == 0:
			totalfound = found
			#return 0
		
		if found > 500:
			if splitlat:
				mapsearch(minlat, (minlat + maxlat)/2, minlong, maxlong, False)
				mapsearch((minlat + maxlat)/2, maxlat, minlong, maxlong, False)
			else:
				mapsearch(minlat, maxlat, (minlong + maxlong)/2, maxlong, True)
				mapsearch(minlat, maxlat, minlong, (minlong + maxlong)/2, True)
		elif found > 0:
			processlist(result,found)
	except:
		print("Unexpected Json error:", sys.exc_info()[0])
		#print(url)
		#print(res.text.replace('\\', ''))


#first search to get total
mapsearch(42.3,70,-150,-45,False)
#mapsearch(-90,90,-180,180,False)
#split up map into array of sections to avoid a huge stack due to recursive.
#for longMin in range(-150,-45,1):
#	for latMin in range(40,70,1):
#		mapsearch(latMin,latMin+1,longMin,longMin+1, False)

#DB SCRIPTS

#db.listings.drop()	
#db.listings.ensureIndex( { "PropertyID": 1 }, {unique : true, dropDups: true} )
#db.listings.find({Price : {$exists : true}}).forEach( function(obj) { obj.Price = obj.Price.replace("$","").replace(",","").replace(",","").trim();obj.Price = ""+obj.Price; db.listings.save(obj); } );
