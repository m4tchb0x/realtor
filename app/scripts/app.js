'use strict';

angular.module('realtorApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'google-maps'
])
.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/map.html',
      controller: 'MapCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
})
.factory('Api', ['$http', function ($http) {
  return {
    getListings: function(bounds, limit, page, qs) {
      var minlat = bounds.southwest.latitude + 1000;
      var maxlat = bounds.northeast.latitude + 1000;
      var minlong = bounds.southwest.longitude + 1000;
      var maxlong = bounds.northeast.longitude + 1000;
      return $http.get('/api/list/'+minlat+'/'+maxlat+'/'+minlong+'/'+maxlong+'/'+limit+'/'+page+'?'+qs).then(function(result) {
        return result.data;
      });
    }
  }
}])
.controller('MapCtrl', ['$scope' , 'Api', function ($scope, Api) {
  $scope.listings = [];
  $scope.map = {
    center: {
      latitude: 45,
      longitude: -73
    },
    zoom: 8,
    bounds: {},
  };

  var onMarkerClicked = function (listing) {
    listing.showWindow = true;
  };
  $scope.onMarkerClicked = onMarkerClicked;

  $scope.search = function () {
    Api.getListings($scope.map.bounds, 1000, 1, $("form").serialize() ).then(function(data) {

      $scope.listings = data.listings;
      console.log($scope.listings);
      _.each($scope.listings, function (listing) {
            listing.latitude = listing.Latitude;
            listing.longitude = listing.Longitude;
            listing.showWindow = false;
            listing.closeClick = function () {
                listing.showWindow = false;
                $scope.$apply();
            };
            listing.onClicked = function () {
                onMarkerClicked(listing);
            };
        });
    });
  };
}]);